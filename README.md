# 2016 Autumn Quarter
* Machine learning [link](https://courses.cs.washington.edu/courses/cse546/16au/)
    * Homework #1 due on Oct 14 
* Robotics [link](http://courses.cs.washington.edu/courses/cse571/16au/)
* AI seminar [link](https://courses.cs.washington.edu/courses/cse590a/16au/)
* CV seminar [link](https://courses.cs.washington.edu/courses/cse590v/16au/)
    * Visit office to make my presentation schedule
* New grad seminar [link](https://courses.cs.washington.edu/courses/cse590e1/16au/)